package kinesis;

import common.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author puneetgill
 */
public class KinesisMain {

    private static final Logger logger = LoggerFactory.getLogger(KinesisMain.class.getSimpleName());

    public static void main(String[] args) {
        KinesisHandler kinesisHandler = new KinesisHandler();

        Path kinesisWildcardPolicyPath = Paths.get("src/main/java/kinesis/iamPolicies/kinesisWildcard.json");
        String kinesisWildcardPolicyString = Utility.readFileAsString(kinesisWildcardPolicyPath);

        Path kinesisEnumeratedPolicyPath = Paths.get("src/main/java/kinesis/iamPolicies/kinesisEnumerated.json");
        String kinesisEnumeratedPolicyString = Utility.readFileAsString(kinesisEnumeratedPolicyPath);

        logger.info("This Invocation of kinesis.startStreamEncryption() should be successful. The policy attached is the following: {}", kinesisWildcardPolicyString);
        kinesisHandler.startStreamEncryption(kinesisWildcardPolicyPath);

        logger.info("This Invocation of kinesis.startStreamEncryption() should be unsuccessful. The policy attached is the following: {}", kinesisEnumeratedPolicyString);
        kinesisHandler.startStreamEncryption(kinesisEnumeratedPolicyPath);

        Path kinesisWildcardRestrictedPolicyPath = Paths.get("src/main/java/kinesis/iamPolicies/kinesisWildcard-Restricted.json");
        String kinesisWildcardRestrictedPolicyString = Utility.readFileAsString(kinesisWildcardRestrictedPolicyPath);

        logger.info("This Invocation of kinesis.startStreamEncryption() should be successful. The policy attached is the following: {}", kinesisWildcardRestrictedPolicyString);
        kinesisHandler.startStreamEncryption(kinesisWildcardRestrictedPolicyPath);

    }

}
