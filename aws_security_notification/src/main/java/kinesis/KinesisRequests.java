package kinesis;

import software.amazon.awssdk.services.kinesis.model.CreateStreamRequest;
import software.amazon.awssdk.services.kinesis.model.DeleteStreamRequest;
import software.amazon.awssdk.services.kinesis.model.EncryptionType;
import software.amazon.awssdk.services.kinesis.model.StartStreamEncryptionRequest;

/**
 * @author puneetgill
 */
public class KinesisRequests {

    public static StartStreamEncryptionRequest startStreamEncryptionRequest(String stream, String keyId) {
        return StartStreamEncryptionRequest.builder()
                .streamName(stream)
                .keyId(keyId)
                .encryptionType(EncryptionType.KMS)
                .build();
    }

    public static CreateStreamRequest createStreamRequest(String stream, int shardCount) {
        return CreateStreamRequest.builder()
                .streamName(stream)
                .shardCount(shardCount)
                .build();
    }

    public static DeleteStreamRequest deleteStreamRequest(String stream) {
        return DeleteStreamRequest.builder()
                .streamName(stream)
                .build();
    }

}
