package kinesis;

import common.Utility;
import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kinesis.KinesisClient;
import software.amazon.awssdk.services.kinesis.model.*;
import sts.StsWrapper;

import java.util.Properties;

/**
 * @author puneetgill
 */
public class KinesisClientWrapper {

    private static final KinesisClient kinesisClient = getClient();
    private static KinesisClientWrapper kinesisClientWrapper = null;

    private KinesisClientWrapper() {}

    public static KinesisClientWrapper getInstance() {
        if(kinesisClientWrapper == null) {
            kinesisClientWrapper = new KinesisClientWrapper();
        }
        return kinesisClientWrapper;
    }

    private static KinesisClient getClient() {
        String configPath = "src/main/resources/config.properties";
        Properties config = Utility.readConfigProperties(configPath);
        AwsSessionCredentials awsSessionCredentials = StsWrapper.getAwsSessionCredentials(config.getProperty("execute-role-arn"));

        return KinesisClient.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsSessionCredentials))
                .region(Region.US_EAST_1)
                .build();
    }


    public StartStreamEncryptionResponse startStreamEncryption(StartStreamEncryptionRequest startStreamEncryptionRequest) {
        return kinesisClient.startStreamEncryption(startStreamEncryptionRequest);
    }

    public CreateStreamResponse createStream(CreateStreamRequest createStreamRequest) {
        return kinesisClient.createStream(createStreamRequest);
    }

    public DeleteStreamResponse deleteStream(DeleteStreamRequest deleteStreamRequest) {
        return kinesisClient.deleteStream(deleteStreamRequest);
    }

}
