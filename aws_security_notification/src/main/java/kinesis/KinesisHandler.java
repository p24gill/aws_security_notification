package kinesis;

import common.Utility;
import iam.IamWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.services.kinesis.model.CreateStreamRequest;
import software.amazon.awssdk.services.kinesis.model.DeleteStreamRequest;
import software.amazon.awssdk.services.kinesis.model.StartStreamEncryptionRequest;
import software.amazon.awssdk.services.kinesis.model.StartStreamEncryptionResponse;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * @author puneetgill
 */
public class KinesisHandler {

    private static final Logger logger = LoggerFactory.getLogger(KinesisHandler.class.getSimpleName());
    private static final String configPropertiesPath = "src/main/resources/config.properties";
    private static final Properties config = Utility.readConfigProperties(configPropertiesPath);

    /**
     * START STREAM ENCRYPTION API
     */
    public void startStreamEncryption(Path executePolicyPath) {
        String stream = config.getProperty("kinesis-data-stream-name");
        String keyId = config.getProperty("key-id");

        setupStartStreamEncryption(executePolicyPath);
        StartStreamEncryptionRequest startStreamEncryptionRequest = KinesisRequests.startStreamEncryptionRequest(stream, keyId);
        try {
            StartStreamEncryptionResponse startStreamEncryptionResponse = KinesisClientWrapper.getInstance()
                    .startStreamEncryption(startStreamEncryptionRequest);
            logger.info("API call successful: {}", startStreamEncryptionResponse.toString());
        } catch(SdkException e) {
            logger.error("API call unsuccessful: {}", e.getMessage());
        }
        teardownStartStreamEncryption();
    }

    private void setupStartStreamEncryption(Path kinesisExecutePolicyPath) {
        detachKinesisExecutePolicy();
        attachKinesisSetupPolicy();

        String stream = config.getProperty("kinesis-data-stream-name");
        int shardCount = Integer.parseInt(config.getProperty("shard-count"));
        CreateStreamRequest createStreamRequest = KinesisRequests.createStreamRequest(stream, shardCount);
        KinesisClientWrapper.getInstance().createStream(createStreamRequest);

        detachKinesisSetupPolicy();
        attachKinesisExecutePolicy(kinesisExecutePolicyPath);
    }

    private void teardownStartStreamEncryption() {
        detachKinesisExecutePolicy();
        attachKinesisSetupPolicy();
        String stream = config.getProperty("kinesis-data-stream-name");
        DeleteStreamRequest deleteStreamRequest = KinesisRequests.deleteStreamRequest(stream);
        KinesisClientWrapper.getInstance().deleteStream(deleteStreamRequest);
        detachKinesisSetupPolicy();
    }


    private void attachKinesisSetupPolicy() {
        Path kinesisSetupPolicyPath = Paths.get("src/main/java/kinesis/iamPolicies/kinesisSetupPolicy.json");
        String executionRole = config.getProperty("execute-role-name");
        String setupEnvPolicyArn = config.getProperty("setup-env-policy-arn");
        attachPolicy(executionRole, kinesisSetupPolicyPath, setupEnvPolicyArn);
    }

    private void detachKinesisSetupPolicy() {
        String executionRole = config.getProperty("execute-role-name");
        String setupEnvPolicyArn = config.getProperty("setup-env-policy-arn");
        detachPolicy(executionRole, setupEnvPolicyArn);
    }


    private void attachPolicy(String executionRole, Path policyPath, String policyArnToAttach) {
        String policyString = Utility.readFileAsString(policyPath);
        IamWrapper iamWrapper = IamWrapper.getInstance();
        iamWrapper.createPolicyVersionAndDeletePreviousVersion(policyArnToAttach, policyString);

        boolean isPolicyAttachedToRole = iamWrapper.isPolicyAttachedToRole(policyArnToAttach, executionRole);
        if(!isPolicyAttachedToRole) {
            iamWrapper.attachRolePolicy(policyArnToAttach, executionRole);
            Utility.waitForMilliSeconds(Integer.parseInt(config.getProperty("milliseconds-to-wait-after-attach-detach-policies")));
        }
    }

    private void detachPolicy(String executionRole, String policyArnToDetach) {
        IamWrapper iamWrapper = IamWrapper.getInstance();
        if(iamWrapper.isPolicyAttachedToRole(policyArnToDetach, executionRole)) {
            iamWrapper.detachRolePolicy(policyArnToDetach, executionRole);
            Utility.waitForMilliSeconds(Integer.parseInt(config.getProperty("milliseconds-to-wait-after-attach-detach-policies")));
        }
    }

    private void attachKinesisExecutePolicy(Path kinesisExecutePolicyPath) {
        String executionRole = config.getProperty("execute-role-name");
        String executePolicyArn = config.getProperty("execute-policy-arn");
        attachPolicy(executionRole, kinesisExecutePolicyPath, executePolicyArn);
    }

    private void detachKinesisExecutePolicy() {
        String executionRole = config.getProperty("execute-role-name");
        String executePolicyArn = config.getProperty("execute-policy-arn");
        detachPolicy(executionRole, executePolicyArn);
    }

}
