package iam;

import common.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.iam.model.*;
import sts.StsWrapper;

import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * @author puneetgill
 */
public class IamWrapper {

    private static Logger logger = LoggerFactory.getLogger(IamWrapper.class.getSimpleName());

    private static IamClient iam = getClient();
    private static IamWrapper iamWrapper = null;

    private IamWrapper() {}

    public static IamWrapper getInstance() {
        if(iamWrapper == null) {
            iamWrapper = new IamWrapper();
        }
        return iamWrapper;
    }

    public DetachRolePolicyResponse detachRolePolicy(String policyArn, String roleName) {
        return iam.detachRolePolicy(DetachRolePolicyRequest.builder()
                .policyArn(policyArn)
                .roleName(roleName)
                .build());
    }

    public AttachRolePolicyResponse attachRolePolicy(String policyArn, String roleName) {
        return iam.attachRolePolicy(AttachRolePolicyRequest.builder()
                .policyArn(policyArn)
                .roleName(roleName)
                .build());
    }

    public ListAttachedRolePoliciesResponse listAttachedRolePolicies(String roleName) {
        return iam.listAttachedRolePolicies(ListAttachedRolePoliciesRequest.builder()
                .roleName(roleName)
                .build());
    }

    public boolean isPolicyAttachedToRole(String policyArn, String roleName) {
        ListAttachedRolePoliciesResponse listAttachedRolePoliciesResponse = listAttachedRolePolicies(roleName);
        if(listAttachedRolePoliciesResponse.attachedPolicies().size() == 0) return false;
        List<String> policyArnsOfAttachedPolicies = listAttachedRolePoliciesResponse.attachedPolicies().stream()
                .map(AttachedPolicy::policyArn)
                .collect(Collectors.toList());

        return policyArnsOfAttachedPolicies.contains(policyArn);
    }

    public CreatePolicyVersionResponse createPolicyVersionAndDeletePreviousVersion(String policyArn, String policyDocument) {
        ListPolicyVersionsResponse listPolicyVersionsResponse = iam.listPolicyVersions(ListPolicyVersionsRequest.builder()
                .policyArn(policyArn)
                .build());
        if(listPolicyVersionsResponse.versions().size() >= 5) {
            deletePolicyVersion(policyArn, listPolicyVersionsResponse.versions().get(1).versionId());
        }

        return iam.createPolicyVersion(CreatePolicyVersionRequest.builder()
                .policyArn(policyArn)
                .policyDocument(policyDocument)
                .setAsDefault(true)
                .build());
    }

    private DeletePolicyVersionResponse deletePolicyVersion(String policyArn, String versionId) {
        return iam.deletePolicyVersion(DeletePolicyVersionRequest.builder()
                .policyArn(policyArn)
                .versionId(versionId)
                .build());
    }

    private static IamClient getClient() {
        String configPath = "src/main/resources/config.properties";
        Properties config = Utility.readConfigProperties(configPath);
        return IamClient.builder()
                .credentialsProvider(StaticCredentialsProvider.create(StsWrapper.getAwsSessionCredentials(config.getProperty("setup-role-arn"))))
                .region(Region.AWS_GLOBAL)
                .build();
    }
}
