package sts;

import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sts.StsClient;
import software.amazon.awssdk.services.sts.model.AssumeRoleRequest;
import software.amazon.awssdk.services.sts.model.AssumeRoleResponse;
import software.amazon.awssdk.services.sts.model.Credentials;

/**
 * @author puneetgill
 */
public class StsWrapper {

    public static AwsSessionCredentials getAwsSessionCredentials(String roleArn) {
        Credentials sessionCredentials = getCredentials(roleArn);
        return AwsSessionCredentials.create(
                sessionCredentials.accessKeyId(),
                sessionCredentials.secretAccessKey(),
                sessionCredentials.sessionToken()
        );
    }

    public static Credentials getCredentials(String roleArn) {
        return assumeRole(roleArn).credentials();
    }

    public static AssumeRoleResponse assumeRole(String roleArn) {
        StsClient sts = getClient();
        return sts.assumeRole(AssumeRoleRequest.builder()
                .roleArn(roleArn)
                .roleSessionName("session1")
                .build());
    }

    private static StsClient getClient() {
        return StsClient.builder()
                .region(Region.US_EAST_1)
                .build();
    }
}
