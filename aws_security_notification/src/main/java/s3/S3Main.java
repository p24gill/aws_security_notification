package s3;

import common.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author puneetgill
 */
public class S3Main {

    private static final Logger logger = LoggerFactory.getLogger(S3Main.class.getSimpleName());

    public static void main(String[] args) {
        S3Handler s3Handler = new S3Handler();

        Path s3GetObjectPolicyPath = Paths.get("src/main/java/s3/iamPolicies/s3GetObject.json");
        String s3GetObjectPolicyString = Utility.readFileAsString(s3GetObjectPolicyPath);

        Path s3GetObjectVersionPolicyPath = Paths.get("src/main/java/s3/iamPolicies/s3GetObjectVersion.json");
        String s3GetObjectVersionPolicyString = Utility.readFileAsString(s3GetObjectVersionPolicyPath);

        logger.info("This Invocation of s3.getObjectAsBytes() with no version Id should be successful. The policy attached is the following: {}", s3GetObjectPolicyString);
        s3Handler.getObjectAsBytes(s3GetObjectPolicyPath);

        logger.info("This Invocation of s3.getObjectAsBytes() with no version Id should be unsuccessful. The policy attached is the following: {}", s3GetObjectVersionPolicyString);
        s3Handler.getObjectAsBytes(s3GetObjectVersionPolicyPath);

        logger.info("This Invocation of s3.getObjectAsBytes() with version Id should be successful. The policy attached is the following: {}", s3GetObjectVersionPolicyString);
        s3Handler.getObjectAsBytesVersioning(s3GetObjectVersionPolicyPath);

        logger.info("This Invocation of s3.getObjectAsBytes() with version Id should be unsuccessful. The policy attached is the following: {}", s3GetObjectPolicyString);
        s3Handler.getObjectAsBytesVersioning(s3GetObjectPolicyPath);

        Path s3CreateBucketPolicyPath = Paths.get("src/main/java/s3/iamPolicies/s3CreateBucket.json");
        String s3CreateBucketPolicyString = Utility.readFileAsString(s3CreateBucketPolicyPath);

        Path s3CreateBucketWithObjectLockEnabledPolicyPath = Paths.get("src/main/java/s3/iamPolicies/s3CreateBucketWithObjectLockEnabled.json");
        String s3CreateBucketWithObjectLockEnabledPolicyString = Utility.readFileAsString(s3CreateBucketWithObjectLockEnabledPolicyPath);

        logger.info("This Invocation of s3.createBucket() with object lock disabled for bucket should be successful. The policy attached is the following: {}", s3CreateBucketPolicyString);
        s3Handler.createBucket(s3CreateBucketPolicyPath);

        logger.info("This Invocation of s3.createBucket() with object lock disabled for bucket should be successful. The policy attached is the following: {}", s3CreateBucketWithObjectLockEnabledPolicyString);
        s3Handler.createBucket(s3CreateBucketWithObjectLockEnabledPolicyPath);

        logger.info("This Invocation of s3.createBucket() with object lock enabled for bucket should be unsuccessful. The policy attached is the following: {}", s3CreateBucketPolicyString);
        s3Handler.createBucketWithObjectLockEnabled(s3CreateBucketPolicyPath);

        logger.info("This Invocation of s3.createBucket() with object lock enabled for bucket should be successful. The policy attached is the following: {}", s3CreateBucketWithObjectLockEnabledPolicyString);
        s3Handler.createBucketWithObjectLockEnabled(s3CreateBucketWithObjectLockEnabledPolicyPath);

    }

}
