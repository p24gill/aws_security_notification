package s3;

import common.Utility;
import iam.IamWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.model.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * @author puneetgill
 */
public class S3Handler {
    private static final Logger logger = LoggerFactory.getLogger(S3Handler.class.getSimpleName());
    private static final String configPropertiesPath = "src/main/resources/config.properties";
    private static final Properties config = Utility.readConfigProperties(configPropertiesPath);

    /**
     * GET OBJECT AS BYTES API
     */
    public void getObjectAsBytes(Path executePolicyPath) {
        String bucket = config.getProperty("bucket");
        String key = config.getProperty("key");

        setupGetObjectAsBytes(executePolicyPath, false);
        GetObjectRequest getObjectRequest = S3Requests.getObjectRequest(bucket, key);
        try {
            GetObjectResponse getObjectResponse = S3ClientWrapper.getInstance().getObjectAsBytes(getObjectRequest);
            logger.info("API call successful: {}", getObjectResponse.toString());
        } catch(SdkException e) {
            logger.error("API call unsuccessful: {}", e.getMessage());
        }
        teardownGetObjectAsBytes();
    }

    public void getObjectAsBytesVersioning(Path executePolicyPath) {
        String bucket = config.getProperty("bucket");
        String key = config.getProperty("key");

        PutObjectResponse putObjectResponse = setupGetObjectAsBytes(executePolicyPath, true);
        GetObjectRequest getObjectRequest = S3Requests.getObjectRequest(bucket, key, putObjectResponse.versionId());
        try {
            GetObjectResponse getObjectResponse = S3ClientWrapper.getInstance().getObjectAsBytes(getObjectRequest);
            logger.info("API call successful: {}", getObjectResponse.toString());
        } catch(SdkException e) {
            logger.error("API call unsuccessful: {}", e.getMessage());
        }
        teardownGetObjectAsBytes();
    }

    private PutObjectResponse setupGetObjectAsBytes(Path s3ExecutePolicyPath, boolean objectLockEnabledForBucket) {
        detachS3ExecutePolicy();
        String bucket = config.getProperty("bucket");
        String key = config.getProperty("key");
        String fileContent = config.getProperty("file-content");

        attachS3SetupPolicy();
        CreateBucketRequest createBucketRequest = S3Requests.createBucketRequest(bucket, objectLockEnabledForBucket);
        S3ClientWrapper.getInstance().createBucket(createBucketRequest);

        Utility.waitForMilliSeconds(Integer.parseInt(config.getProperty("milliseconds-to-wait-after-attach-detach-policies")));

        PutObjectRequest putObjectRequest = S3Requests.putObjectRequest(bucket, key);
        RequestBody requestBody = RequestBody.fromString(fileContent);
        PutObjectResponse putObjectResponse = S3ClientWrapper.getInstance().putObject(putObjectRequest, requestBody);
        detachS3SetupPolicy();
        attachS3ExecutePolicy(s3ExecutePolicyPath);
        return putObjectResponse;
    }

    private void teardownGetObjectAsBytes() {
        String bucket = config.getProperty("bucket");
        detachS3ExecutePolicy();
        attachS3SetupPolicy();
        S3ClientWrapper.getInstance().emptyBucketAndDelete(bucket);
        detachS3SetupPolicy();
    }


    /**
     * CREATE BUCKET API
     */
    public void createBucket(Path executePolicyPath) {
        String bucket = config.getProperty("bucket");

        setupCreateBucket(executePolicyPath);
        CreateBucketRequest createBucketRequest = S3Requests.createBucketRequest(bucket, false);
        try {
            CreateBucketResponse createBucketResponse = S3ClientWrapper.getInstance().createBucket(createBucketRequest);
            logger.info("API call successful: {}", createBucketResponse.toString());
            teardownCreateBucket();
        } catch(SdkException e) {
            logger.error("API call unsuccessful: {}", e.getMessage());
            detachS3ExecutePolicy();
        }
    }

    public void createBucketWithObjectLockEnabled(Path executePolicyPath) {
        String bucket = config.getProperty("bucket");

        setupCreateBucket(executePolicyPath);
        CreateBucketRequest createBucketRequest = S3Requests.createBucketRequest(bucket, true);
        try {
            CreateBucketResponse createBucketResponse = S3ClientWrapper.getInstance().createBucket(createBucketRequest);
            logger.info("API call successful: {}", createBucketResponse.toString());
            teardownCreateBucket();
        } catch(SdkException e) {
            logger.error("API call unsuccessful: {}", e.getMessage());
            detachS3ExecutePolicy();
        }
    }

    private void setupCreateBucket(Path s3ExecutePolicyPath) {
        detachS3SetupPolicy();
        attachS3ExecutePolicy(s3ExecutePolicyPath);
    }

    private void teardownCreateBucket() {
        String bucket = config.getProperty("bucket");
        detachS3ExecutePolicy();
        attachS3SetupPolicy();
        S3ClientWrapper.getInstance().emptyBucketAndDelete(bucket);
        detachS3SetupPolicy();
    }

    private void attachS3SetupPolicy() {
        Path s3SetupPolicyPath = Paths.get("src/main/java/s3/iamPolicies/s3SetupPolicy.json");
        String executionRole = config.getProperty("execute-role-name");
        String setupEnvPolicyArn = config.getProperty("setup-env-policy-arn");
        attachPolicy(executionRole, s3SetupPolicyPath, setupEnvPolicyArn);
    }

    private void detachS3SetupPolicy() {
        String executionRole = config.getProperty("execute-role-name");
        String setupEnvPolicyArn = config.getProperty("setup-env-policy-arn");
        detachPolicy(executionRole, setupEnvPolicyArn);
    }


    private void attachPolicy(String executionRole, Path policyPath, String policyArnToAttach) {
        String policyString = Utility.readFileAsString(policyPath);
        IamWrapper iamWrapper = IamWrapper.getInstance();
        iamWrapper.createPolicyVersionAndDeletePreviousVersion(policyArnToAttach, policyString);

        boolean isPolicyAttachedToRole = iamWrapper.isPolicyAttachedToRole(policyArnToAttach, executionRole);
        if(!isPolicyAttachedToRole) {
            iamWrapper.attachRolePolicy(policyArnToAttach, executionRole);
            Utility.waitForMilliSeconds(Integer.parseInt(config.getProperty("milliseconds-to-wait-after-attach-detach-policies")));
        }
    }

    private void detachPolicy(String executionRole, String policyArnToDetach) {
        IamWrapper iamWrapper = IamWrapper.getInstance();
        if(iamWrapper.isPolicyAttachedToRole(policyArnToDetach, executionRole)) {
            iamWrapper.detachRolePolicy(policyArnToDetach, executionRole);
            Utility.waitForMilliSeconds(Integer.parseInt(config.getProperty("milliseconds-to-wait-after-attach-detach-policies")));
        }
    }

    private void attachS3ExecutePolicy(Path s3ExecutePolicyPath) {
        String executionRole = config.getProperty("execute-role-name");
        String executePolicyArn = config.getProperty("execute-policy-arn");
        attachPolicy(executionRole, s3ExecutePolicyPath, executePolicyArn);
    }

    private void detachS3ExecutePolicy() {
        String executionRole = config.getProperty("execute-role-name");
        String executePolicyArn = config.getProperty("execute-policy-arn");
        detachPolicy(executionRole, executePolicyArn);
    }



}
