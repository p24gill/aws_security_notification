[main] INFO S3Main - This Invocation of s3.getObjectAsBytes() with no version Id should be successful. The policy attached is the following: {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:getObject",
      "Resource": "arn:aws:s3:::test-bucket-5457/test-folder/file.txt"
    }
  ]
}
[main] INFO S3Handler - API call successful: GetObjectResponse(AcceptRanges=bytes, LastModified=2020-04-29, ContentLength=7, ETag={etag}, ContentType=text/plain; charset=UTF-8, Metadata={})
[main] INFO S3Main - This Invocation of s3.getObjectAsBytes() with no version Id should be unsuccessful. The policy attached is the following: {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:getObjectVersion",
      "Resource": "arn:aws:s3:::test-bucket-5457/test-folder/file.txt"
    }
  ]
}
[main] ERROR S3Handler - API call unsuccessful: Access Denied (Service: S3, Status Code: 403, Request ID: {request-id})
[main] INFO S3Main - This Invocation of s3.getObjectAsBytes() with version Id should be successful. The policy attached is the following: {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:getObjectVersion",
      "Resource": "arn:aws:s3:::test-bucket-5457/test-folder/file.txt"
    }
  ]
}
[main] INFO S3Handler - API call successful: GetObjectResponse(AcceptRanges=bytes, LastModified=2020-04-29, ContentLength=7, ETag="{etag}", VersionId={version-id}, ContentType=text/plain; charset=UTF-8, Metadata={})
[main] INFO S3Main - This Invocation of s3.getObjectAsBytes() with version Id should be unsuccessful. The policy attached is the following: {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:getObject",
      "Resource": "arn:aws:s3:::test-bucket-5457/test-folder/file.txt"
    }
  ]
}
[main] ERROR S3Handler - API call unsuccessful: Access Denied (Service: S3, Status Code: 403, Request ID: {request-id})

Process finished with exit code 0
