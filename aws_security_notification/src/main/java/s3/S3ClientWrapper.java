package s3;

import common.Utility;
import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.utils.CollectionUtils;
import sts.StsWrapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

/**
 * @author puneetgill
 */
public class S3ClientWrapper {

    private static final String configPropertiesPath = "src/main/resources/config.properties";
    private static final Properties config = Utility.readConfigProperties(configPropertiesPath);

    private static final S3Client s3Client = getClient();
    private static S3ClientWrapper s3ClientWrapper = null;

    private S3ClientWrapper() {}

    public static S3ClientWrapper getInstance() {
        if(s3ClientWrapper == null) {
            s3ClientWrapper = new S3ClientWrapper();
        }
        return s3ClientWrapper;
    }

    private static S3Client getClient() {
        String configPath = "src/main/resources/config.properties";
        Properties config = Utility.readConfigProperties(configPath);
        AwsSessionCredentials awsSessionCredentials = StsWrapper.getAwsSessionCredentials(config.getProperty("execute-role-arn"));

        return S3Client.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsSessionCredentials))
                .region(Region.US_EAST_1)
                .build();
    }

    public GetObjectResponse getObjectAsBytes(GetObjectRequest getObjectRequest) {
        return s3Client.getObject(getObjectRequest).response();
    }


    public CreateBucketResponse createBucket(CreateBucketRequest createBucketRequest) {
        return s3Client.createBucket(createBucketRequest);
    }

    public PutObjectResponse putObject(PutObjectRequest putObjectRequest, RequestBody requestBody) {
        return s3Client.putObject(putObjectRequest, requestBody);
    }


    public void emptyBucketAndDelete(String bucketName) {
        GetBucketVersioningResponse getBucketVersioningResponse = s3Client.getBucketVersioning(GetBucketVersioningRequest.builder()
                .bucket(bucketName)
                .build());
        if(getBucketVersioningResponse.status() != null && getBucketVersioningResponse.status().equals(BucketVersioningStatus.ENABLED)) {
            deleteBucketWithVersioning(bucketName);
        } else {
            deleteBucketWithoutVersioning(bucketName);
        }
    }

    private void deleteBucketWithoutVersioning(String bucketName) {
        ListObjectsResponse listObjectsResponse = s3Client.listObjects(ListObjectsRequest.builder()
                .bucket(bucketName)
                .build());
        for (S3Object s3Object : listObjectsResponse.contents()) {
            DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder()
                    .bucket(bucketName)
                    .key(s3Object.key())
                    .build();
            s3Client.deleteObject(deleteObjectRequest);
        }
        s3Client.deleteBucket(DeleteBucketRequest.builder()
                .bucket(bucketName)
                .build());
    }

    private void deleteBucketWithVersioning(String bucketName) {
        ListObjectVersionsResponse listObjectVersionsResponse = s3Client.listObjectVersions(ListObjectVersionsRequest.builder()
                .bucket(bucketName)
                .build());

        if(!CollectionUtils.isNullOrEmpty(listObjectVersionsResponse.versions())) {
            Collection<ObjectIdentifier> objectIdentifiers = new ArrayList<>();
            for(ObjectVersion objectVersion : listObjectVersionsResponse.versions()) {
                ObjectIdentifier objectIdentifier = ObjectIdentifier.builder()
                        .key(objectVersion.key())
                        .versionId(objectVersion.versionId())
                        .build();
                objectIdentifiers.add(objectIdentifier);

            }
            Delete objectsToDelete = Delete.builder()
                    .objects(objectIdentifiers)
                    .quiet(true)
                    .build();
            DeleteObjectsRequest deleteObjectsRequest = DeleteObjectsRequest.builder()
                    .bucket(bucketName)
                    .delete(objectsToDelete)
                    .build();
            s3Client.deleteObjects(deleteObjectsRequest);
            Utility.waitForMilliSeconds(Integer.parseInt(config.getProperty("milliseconds-to-wait-after-attach-detach-policies")));
        }
        s3Client.deleteBucket(DeleteBucketRequest.builder()
                .bucket(bucketName)
                .build());
    }



}
