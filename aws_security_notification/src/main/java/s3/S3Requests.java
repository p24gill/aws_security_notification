package s3;

import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

/**
 * @author puneetgill
 */
public class S3Requests {

    public static GetObjectRequest getObjectRequest(String bucket, String key) {
        return GetObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .build();
    }

    public static GetObjectRequest getObjectRequest(String bucket, String key, String versionId) {
        return GetObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .versionId(versionId)
                .build();
    }


    public static CreateBucketRequest createBucketRequest(String bucket, boolean objectLockEnabledForBucket) {
        return CreateBucketRequest.builder()
                .bucket(bucket)
                .objectLockEnabledForBucket(objectLockEnabledForBucket)
                .build();
    }

    public static PutObjectRequest putObjectRequest(String bucket, String key) {
        return PutObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .build();
    }

}
