package common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

/**
 * @author puneetgill
 */
public class Utility {

    private static final Logger logger = LoggerFactory.getLogger(Utility.class.getSimpleName());

    public static void waitForMilliSeconds(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String readFileAsString(Path filePath) {
        String content = null;
        try {
            content = Files.readString(filePath);
        } catch (IOException e) {
            logger.error("Failed to read the file: {}. Error: {}", filePath.toString(), e.getMessage());
        }
        return content;
    }

    public static Properties readConfigProperties(String configPropertiesPath) {
        Properties properties = new Properties();
        try {
            FileInputStream config = new FileInputStream(configPropertiesPath);
            properties.load(config);
        } catch (FileNotFoundException e) {
            logger.error("Configuration file: {} not found. Exception: {}", configPropertiesPath, e.getMessage());
        } catch (IOException e) {
            logger.error("Failed to load configuration properties. Exception: {}", e.getMessage());
        }
        return properties;
    }

}
