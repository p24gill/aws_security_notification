package ec2;

import software.amazon.awssdk.services.ec2.model.*;

public class EC2Requests {

    public static CreateImageRequest createImageRequest(String instanceId, String imageName) {
        return CreateImageRequest.builder()
                .instanceId(instanceId)
                .name(imageName)
                .build();
    }

    public static ExportImageRequest exportImageRequest(String bucket, String prefix, String serviceRole, String imageId) {
        return ExportImageRequest.builder()
                .roleName(serviceRole)
                .s3ExportLocation(exportTaskS3LocationRequest(bucket, prefix))
                .diskImageFormat(DiskImageFormat.VMDK)
                .imageId(imageId)
                .build();
    }


    private static ExportTaskS3LocationRequest exportTaskS3LocationRequest(String bucketName, String prefix) {
        return ExportTaskS3LocationRequest.builder()
                .s3Bucket(bucketName)
                .s3Prefix(prefix)
                .build();
    }


    public static CancelExportTaskRequest cancelExportTaskRequest(String exportTaskId) {
        return CancelExportTaskRequest.builder()
                .exportTaskId(exportTaskId)
                .build();
    }

    public static DeregisterImageRequest deregisterImageRequest(String imageId) {
        return DeregisterImageRequest.builder()
                .imageId(imageId)
                .build();
    }



}
