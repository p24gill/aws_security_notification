package ec2;

import common.Utility;

import iam.IamWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import s3.S3ClientWrapper;
import s3.S3Requests;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.services.ec2.model.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class EC2Handler {

    private static final Logger logger = LoggerFactory.getLogger(EC2Handler.class.getSimpleName());

    private static final String configPropertiesPath = "src/main/resources/config.properties";
    private static final Properties config = Utility.readConfigProperties(configPropertiesPath);

    /** EXPORT IMAGE **/
    public void exportImage(Path executePolicyPath, Path ec2ServiceRolePolicyPath) {
        String instanceId = config.getProperty("instance-id");
        String imageName = config.getProperty("image-name");
        String ec2ServiceRole = config.getProperty("ec2-service-role-name");
        String exportBucket = config.getProperty("export-bucket");
        String prefix = config.getProperty("prefix");

        CreateImageResponse createImageResponse = setupExportImage(instanceId, imageName, exportBucket, executePolicyPath, ec2ServiceRolePolicyPath);

        try {
            ExportImageRequest exportImageRequest = EC2Requests.exportImageRequest(exportBucket, prefix, ec2ServiceRole, createImageResponse.imageId());
            ExportImageResponse exportImageResponse = EC2ClientWrapper.getInstance().exportImage(exportImageRequest);
            logger.info("API call successful: {}", exportImageResponse.toString());

            tearDownExportImage(exportImageResponse.exportImageTaskId(), createImageResponse.imageId(), exportBucket);
        } catch(SdkException e) {
            logger.error("API call unsuccessful: {}", e.getMessage());
            tearDownExportImage(createImageResponse.imageId(), exportBucket);
        }
    }

    public void exportImage(Path executePolicyPath) {
        String instanceId = config.getProperty("instance-id");
        String imageName = config.getProperty("image-name");
        String ec2ServiceRole = config.getProperty("ec2-service-role-name");
        String exportBucket = config.getProperty("export-bucket");
        String prefix = config.getProperty("prefix");

        CreateImageResponse createImageResponse = setupExportImage(instanceId, imageName, exportBucket, executePolicyPath);

        try {
            ExportImageRequest exportImageRequest = EC2Requests.exportImageRequest(exportBucket, prefix, ec2ServiceRole, createImageResponse.imageId());
            ExportImageResponse exportImageResponse = EC2ClientWrapper.getInstance().exportImage(exportImageRequest);
            logger.info("API call successful: {}", exportImageResponse.toString());

            tearDownExportImage(exportImageResponse.exportImageTaskId(), createImageResponse.imageId(), exportBucket);
        } catch(SdkException e) {
            logger.error("API call unsuccessful: {}", e.getMessage());
            tearDownExportImage(createImageResponse.imageId(), exportBucket);
        }
    }

    private CreateImageResponse setupExportImage(String instanceId, String imageName, String exportBucket, Path ec2ExecutePolicyPath) {
        detachEC2ExecutePolicy();
        attachEC2SetupPolicy();

        CreateImageRequest createImageRequest = EC2Requests.createImageRequest(instanceId, imageName);
        CreateImageResponse createImageResponse = EC2ClientWrapper.getInstance().createImage(createImageRequest);
        S3ClientWrapper.getInstance().createBucket(S3Requests.createBucketRequest(exportBucket, false));
        Utility.waitForMilliSeconds(10000);

        detachEC2SetupPolicy();
        attachEC2ExecutePolicy(ec2ExecutePolicyPath);
        return createImageResponse;
    }

    private CreateImageResponse setupExportImage(String instanceId, String imageName,  String exportBucket, Path ec2ExecutePolicyPath, Path ec2ServiceRolePolicyPath) {
        CreateImageResponse createImageResponse = setupExportImage(instanceId, imageName, exportBucket, ec2ExecutePolicyPath);
        attachEC2ServiceRolePolicy(ec2ServiceRolePolicyPath);
        return createImageResponse;
    }

    private void tearDownExportImage(String exportTaskId, String imageId, String exportBucket) {
        detachEC2ServiceRolePolicy();
        detachEC2ExecutePolicy();
        attachEC2SetupPolicy();

        Utility.waitForMilliSeconds(60000);
        try {
            EC2ClientWrapper.getInstance().cancelExportTask(EC2Requests.cancelExportTaskRequest(exportTaskId));
        } catch(AwsServiceException e) {
            logger.error("Failed to cancel Export Task: {}. Going to deregister the AMI and delete export bucket", e.getMessage());
        }
        Utility.waitForMilliSeconds(30000);
        EC2ClientWrapper.getInstance().deregisterImage(EC2Requests.deregisterImageRequest(imageId));
        S3ClientWrapper.getInstance().emptyBucketAndDelete(exportBucket);

        detachEC2SetupPolicy();
    }

    private void tearDownExportImage(String imageId, String exportBucket) {
        detachEC2ServiceRolePolicy();
        detachEC2ExecutePolicy();
        attachEC2SetupPolicy();

        EC2ClientWrapper.getInstance().deregisterImage(EC2Requests.deregisterImageRequest(imageId));
        S3ClientWrapper.getInstance().emptyBucketAndDelete(exportBucket);

        detachEC2SetupPolicy();
    }


    private void attachEC2SetupPolicy() {
        Path ec2SetupPolicyPath = Paths.get("src/main/java/ec2/iamPolicies/ec2SetupPolicy.json");
        String executionRole = config.getProperty("execute-role-name");
        String setupEnvPolicyArn = config.getProperty("setup-env-policy-arn");
        attachPolicy(executionRole, ec2SetupPolicyPath, setupEnvPolicyArn);
    }

    private void detachEC2SetupPolicy() {
        String executionRole = config.getProperty("execute-role-name");
        String setupEnvPolicyArn = config.getProperty("setup-env-policy-arn");
        detachPolicy(executionRole, setupEnvPolicyArn);
    }


    private void attachPolicy(String executionRole, Path policyPath, String policyArnToAttach) {
        String policyString = Utility.readFileAsString(policyPath);
        IamWrapper iamWrapper = IamWrapper.getInstance();
        iamWrapper.createPolicyVersionAndDeletePreviousVersion(policyArnToAttach, policyString);

        boolean isPolicyAttachedToRole = iamWrapper.isPolicyAttachedToRole(policyArnToAttach, executionRole);
        if(!isPolicyAttachedToRole) {
            iamWrapper.attachRolePolicy(policyArnToAttach, executionRole);
            Utility.waitForMilliSeconds(Integer.parseInt(config.getProperty("milliseconds-to-wait-after-attach-detach-policies")));
        }
    }

    private void detachPolicy(String executionRole, String policyArnToDetach) {
        IamWrapper iamWrapper = IamWrapper.getInstance();
        if(iamWrapper.isPolicyAttachedToRole(policyArnToDetach, executionRole)) {
            iamWrapper.detachRolePolicy(policyArnToDetach, executionRole);
            Utility.waitForMilliSeconds(Integer.parseInt(config.getProperty("milliseconds-to-wait-after-attach-detach-policies")));
        }
    }

    private void attachEC2ExecutePolicy(Path ec2ExecutePolicyPath) {
        String executionRole = config.getProperty("execute-role-name");
        String executePolicyArn = config.getProperty("execute-policy-arn");
        attachPolicy(executionRole, ec2ExecutePolicyPath, executePolicyArn);
    }

    private void detachEC2ExecutePolicy() {
        String executionRole = config.getProperty("execute-role-name");
        String executePolicyArn = config.getProperty("execute-policy-arn");
        detachPolicy(executionRole, executePolicyArn);
    }


    private void attachEC2ServiceRolePolicy(Path ec2ExecutePolicyPath) {
        String ec2ServicenRole = config.getProperty("ec2-service-role-name");
        String ec2ServiceRolePolicyArn = config.getProperty("ec2-service-role-policy-arn");
        attachPolicy(ec2ServicenRole, ec2ExecutePolicyPath, ec2ServiceRolePolicyArn);
    }

    private void detachEC2ServiceRolePolicy() {
        String ec2ServicenRole = config.getProperty("ec2-service-role-name");
        String ec2ServiceRolePolicyArn = config.getProperty("ec2-service-role-policy-arn");
        detachPolicy(ec2ServicenRole, ec2ServiceRolePolicyArn);
    }

}
