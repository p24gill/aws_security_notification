package ec2;

import common.Utility;

import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.*;
import sts.StsWrapper;

import java.util.Properties;

/**
 * @author puneetgill
 */
public class EC2ClientWrapper {

    private static final Ec2Client ec2Client = getClient();
    private static EC2ClientWrapper ec2ClientWrapper = null;

    private EC2ClientWrapper() {}

    public static EC2ClientWrapper getInstance() {
        if(ec2ClientWrapper == null) {
            ec2ClientWrapper = new EC2ClientWrapper();
        }
        return ec2ClientWrapper;
    }

    private static Ec2Client getClient() {
        String configPath = "src/main/resources/config.properties";
        Properties config = Utility.readConfigProperties(configPath);
        AwsSessionCredentials awsSessionCredentials = StsWrapper.getAwsSessionCredentials(config.getProperty("execute-role-arn"));

        return Ec2Client.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsSessionCredentials))
                .region(Region.US_EAST_1)
                .build();
    }

    public CreateImageResponse createImage(CreateImageRequest createImageRequest) {
        return ec2Client.createImage(createImageRequest);
    }

    public DeregisterImageResponse deregisterImage(DeregisterImageRequest deregisterImageRequest) {
        return ec2Client.deregisterImage(deregisterImageRequest);
    }

    public ExportImageResponse exportImage(ExportImageRequest exportImageRequest) {
        return ec2Client.exportImage(exportImageRequest);
    }

    public CancelExportTaskResponse cancelExportTask(CancelExportTaskRequest cancelExportTaskRequest) {
        return ec2Client.cancelExportTask(cancelExportTaskRequest);
    }

}
