package ec2;

import common.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;

public class EC2Main {

    private static final Logger logger = LoggerFactory.getLogger(EC2Main.class.getSimpleName());

    public static void main(String[] args) {
        EC2Handler ec2Handler = new EC2Handler();

        Path ec2ExportImagePolicyPath = Paths.get("src/main/java/ec2/iamPolicies/ec2ExportImage.json");
        String ec2ExportImagePolicyString = Utility.readFileAsString(ec2ExportImagePolicyPath);

        Path ec2ExportImageServiceRolePolicyPath = Paths.get("src/main/java/ec2/iamPolicies/ec2ExportImageServiceRole.json");
        String ec2ExportImageServiceRolePolicyString = Utility.readFileAsString(ec2ExportImageServiceRolePolicyPath);


        logger.info("This Invocation of ec2.exportImage() should be successful. " +
                "The policy attached is the following: {} and the following policy is attached to the service role: {}",
                ec2ExportImagePolicyString, ec2ExportImageServiceRolePolicyString);
        ec2Handler.exportImage(ec2ExportImagePolicyPath, ec2ExportImageServiceRolePolicyPath);

        logger.info("This Invocation of ec2.exportImage() should be unsuccessful. " +
                "The policy attached is the following: {} and no policy is attached to the service role.",
                ec2ExportImagePolicyString);
        ec2Handler.exportImage(ec2ExportImagePolicyPath);

    }

}
