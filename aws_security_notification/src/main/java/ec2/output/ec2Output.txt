[main] INFO EC2Main - This Invocation of ec2.exportImage() should be successful. The policy attached is the following: {
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "ec2:ExportImage",
    "Resource": "*"
  }
} and the following policy is attached to the service role: {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetBucketAcl"
      ],
      "Resource": [
        "arn:aws:s3:::export-bucket-5457",
        "arn:aws:s3:::export-bucket-5457/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": "ec2:DescribeImages",
      "Resource": "*"
    }
  ]
}
[main] INFO EC2Handler - API call successful: ExportImageResponse(DiskImageFormat=vmdk, ExportImageTaskId={export-task-id}, ImageId={ami-image-id}, RoleName=ec2-service-role, Progress=0, S3ExportLocation=ExportTaskS3Location(S3Bucket=export-bucket-5457, S3Prefix=exports/), Status=active, StatusMessage=validating)
[main] INFO EC2Main - This Invocation of ec2.exportImage() should be unsuccessful. The policy attached is the following: {
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "ec2:ExportImage",
    "Resource": "*"
  }
} and no policy is attached to the service role.
[main] ERROR EC2Handler - API call unsuccessful: The service role ec2-service-role provided does not exist or does not have sufficient permissions (Service: Ec2, Status Code: 400, Request ID: {request-id})

Process finished with exit code 0
